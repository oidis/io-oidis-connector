/* ********************************************************************************************************* *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.Oidis.Builder.DAO.Resources.Data({
    $interface: "IProjectSolution",
    name: "io-oidis-connector",
    description: "Standalone HTTP server for Oidis Framework applications with WebSockets support and built-in web host",
    version: "2025.0.0",
    homepage: "www.oidis.io",
    repository: "https://gitlab.com/oidis/io-oidis-connector.git",
    author: {
        name: "Oidis z.s.",
        email: "info@oidis.io"
    },
    license: "Copyright 2019-<? @var build.year ?> Oidis. The BSD-3-Clause license for this file can be found in the LICENSE.txt " +
        "file included with this distribution or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText.",
    dependencies: {
        "io-oidis-localhost": "git+https://gitlab.com/oidis/io-oidis-localhost.git#integration"
    },
    loaderClass: "Io.Oidis.Connector.Loader",
    guiLoaderClass: "Io.Oidis.Connector.Gui.Loader",
    imports: {
        commons: "bin/project/configs/commons.jsonp",
        licensesRegister: "bin/project/configs/licensesRegister.jsonp",
        deploy: "bin/project/configs/deploy.jsonp"
    },
    target: {
        name: "OidisConnector",
        copyright: "Copyright 2019-<? @var build.year ?> Oidis",
        description: "Standalone HTTP server, which provides access to local machine"
    },
    solutions: {
        default: "static",
        static: "bin/project/solutions/static.jsonp",
        shared: "bin/project/solutions/shared.jsonp",
        app: "bin/project/solutions/app.jsonp"
    }
});
