/* ********************************************************************************************************* *
 *
 * Copyright 2020-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.Oidis.Builder.DAO.Resources.Data({
    $interface: "IProjectSolution",
    extendsConfig: "dependencies/io-oidis-localhost/bin/project/configs/commons.jsonp",
    nodejsConfig: {
        devDependencies: {
            "@types/ref": "^0.0.32",
            hidefile: "^3.0.0",
            "get-port": "^7.0.0",
            "is-port-free": "^1.0.7"
        },
        optionalDependencies: {
            "windows-shortcuts": "^0.1.6"
        },
        pkg: {
            "io-oidis-localhost": {
                patches: {
                    bindings: {
                        enabled: false
                    }
                }
            },
            "io-oidis-connector": {
                configCwd: "bin/project/configs",
                patches: {
                    bindings: {
                        src: "resource/scripts/bindings_patch.js",
                        dest: "bindings/bindings.js"
                    },
                    "windows-shortcuts": {
                        src: "resource/scripts/windows-shortcuts_patch.js",
                        dest: "windows-shortcuts/lib/windows-shortcuts.js"
                    }
                }
            }
        }
    },
    domain: {
        location: ""
    },
    hubLocations: {
        prod: "https://hub.oidis.io",
        dev: "https://hub.dev.oidis.io",
        eap: "https://hub.eap.oidis.io"
    },
    agent: {
        hub: "prod"
    },
    connectorProxy: false,
    heartbeat: {
        enabled: true,
        exitOnError: false,
        period: 30000,
        timeout: 5000,
        restartDelay: 5000
    }
});
