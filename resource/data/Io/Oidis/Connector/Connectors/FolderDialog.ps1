#/* ********************************************************************************************************* *
# *
# * Copyright 2020 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */

param([string]$initDir, [string]$path, [string]$title, [bool]$openOnly)

Add-Type -AssemblyName System.Windows.Forms
$dialog = new-object Windows.Forms.FolderBrowserDialog
$dialog.SelectedPath = $path
$dialog.RootFolder = $initDir
$dialog.Description = $title
$dialog.ShowNewFolderButton = !$openOnly

$result = $dialog.ShowDialog((new-object System.Windows.Forms.Form -Property @{TopMost = $True; TopLevel = $True}))
if ($result -eq "OK") {
    $dialog.SelectedPath
}
