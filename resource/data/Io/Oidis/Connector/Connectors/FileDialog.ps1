#/* ********************************************************************************************************* *
# *
# * Copyright 2020 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */

param([string]$initDir, [string]$path, [string]$title, [string]$filter, [Int]$filterIndex, [bool]$multiselect, [bool]$openOnly)

Add-Type -AssemblyName System.Windows.Forms
$dialog = new-object Windows.Forms.OpenFileDialog
$dialog.FileName = $path
$dialog.InitialDirectory = $initDir
$dialog.Filter = $filter
$dialog.FilterIndex = $filterIndex
$dialog.Title = $title
$dialog.Multiselect = $multiselect
$dialog.ReadOnlyChecked = $openOnly

$result = $dialog.ShowDialog((new-object System.Windows.Forms.Form -Property @{TopMost = $True; TopLevel = $True}))
if ($result -eq "OK") {
    if ($dialog.Multiselect) {
        $dialog.FileNames
    } else {
        $dialog.FileName
    }
}
