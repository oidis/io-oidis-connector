' * ********************************************************************************************************* *
' *
' * Copyright 2020-2025 Oidis
' *
' * SPDX-License-Identifier: BSD-3-Clause
' * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
' * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
' *
' * ********************************************************************************************************* */

Set args = CreateObject("Scripting.Dictionary")
For Each oItem In Wscript.Arguments: args.Add oItem, "": Next

Set fso = CreateObject("Scripting.FileSystemObject")
pipe = fso.GetSpecialFolder(2) & "\wscript_pipe_" & Mid(CreateObject("Scriptlet.TypeLib").Guid, 2, 36)

Return = CreateObject("Wscript.Shell").Run("powershell -executionpolicy bypass -Command " & Join(args.Keys(), """ """) & "> " & pipe, 0, true)

Set pipeObj = fso.GetFile(pipe)
If pipeObj.Size > 0 Then
Set file = fso.OpenTextFile(pipe, 1)
text = file.ReadAll
file.Close
On Error Resume Next
WScript.StdOut.Write(text)
Err.Clear
End If

fso.DeleteFile pipe
