#/* ********************************************************************************************************* *
# *
# * Copyright 2020 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */

param([string]$initDir, [string]$path, [string]$title, [string]$filter, [Int]$filterIndex, [string]$extension,
[bool]$autoExtension, [bool]$restore, [bool]$override)

Add-Type -AssemblyName System.Windows.Forms
$dialog = new-object Windows.Forms.SaveFileDialog
$dialog.FileName = $path
$dialog.InitialDirectory = $initDir
$dialog.Filter = $filter
$dialog.FilterIndex = $filterIndex
$dialog.Title = $title
$dialog.AddExtension = $autoExtension
$dialog.DefaultExt = $extension
$dialog.RestoreDirectory = $restore
$dialog.OverwritePrompt = !$override

$result = $dialog.ShowDialog((new-object System.Windows.Forms.Form -Property @{TopMost = $True; TopLevel = $True}))
if ($result -eq "OK") {
    $dialog.FileName
}
