/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Io.Oidis.Builder.Loader;
const LogIt = Io.Oidis.Commons.Utils.LogIt;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const DependenciesInstall = Io.Oidis.Builder.Tasks.Composition.DependenciesInstall;
const StringUtils = Io.Oidis.Commons.Utils.StringUtils;
const OSType = Io.Oidis.Builder.Enums.OSType;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();

let cwd;

function validate($done) {
    if (filesystem.Exists(cwd + "/build/Release/drivelist.node")) {
        CheckModule(cwd + "/build/Release/drivelist.node", $done);
    } else {
        $done(false);
    }
}

function build($done) {
    const env = process.env;
    env.CC = "aarch64-linux-gnu-gcc";
    env.CXX = "aarch64-linux-gnu-g++";
    env.LINK = "aarch64-linux-gnu-g++";
    env.RANLIB = "aarch64-linux-gnu-ranlib";
    env.AR = "aarch64-linux-gnu-ar";

    env.CC_host = "gcc";
    env.CXX_host = "g++";
    env.LINK_host = "g++";
    env.RANLIB_host = "ranlib";
    env.AR_host = "ar";

    terminal.Spawn(EnvironmentHelper.getNodejsRoot() + "/node_modules/npm/bin/node-gyp-bin/node-gyp",
        ["rebuild", "--dest-cpu=arm64", "--dest-os=linux", "--cross-compiling", "--target_arch=arm64"],
        {cwd: cwd, env: env},
        ($exitCode) => {
            if ($exitCode === 0) {
                $done();
            } else {
                LogIt.Error("Can not cross-compile drivelist");
            }
        });
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
        validate(($status) => {
            if ($status !== true) {
                LogIt.Info("Starting drivelist build...");
                build($done);
            } else {
                LogIt.Info("Drivelist is ready.");
                $done();
            }
        })
    } else {
        LogIt.Info("Drivelist build script nothing to build on " +
            DependenciesInstall.getCurrentInstallOsType() + " platform.");
        $done();
    }
};
