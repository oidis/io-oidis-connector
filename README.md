# io-oidis-connector

> Standalone HTTP server for Oidis Framework applications with WebSockets support and built-in web host

## Requirements

This project does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

## History

### v2022.2.0
Reflect changes in dependencies.
### v2022.1.0
Integrated new agent's metadata info. Minor modules cleanup based on integrity check results.
### v2022.0.0
Agent registration fixes and cleanup. 
### v2021.2.0
Integrated novel logger. Fixed bug connected with IntelliJ indexing after IDEA update. Fixed dialog default location support on MacOS.
### v2021.0.0
Release to baseline changes in dependencies. Updated NPM modules and use our fastcall fork.
### v2020.3.0
Added support for native dialogs. Reflected changes in io-oidis-localhost dependency.
### v2020.1.0
Minor cleanup. Reflect changes in dependencies.
### v2020.0.1
Identity update. Change of configuration files format.
### v2020.0.0
Use relative path for ARM compiler in build scripts for NPM native modules.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork. 
Migration to solutions from releases. Added full support for OSX and Linux. Implemented file transfer protocol. 
Added ability to create tunnel for any port and protocol. 
### v2019.1.0
Enable to use connector in role of application updater. FFIProxy refactoring and updates. Usage of decorators for extern API. 
Usage of typedefs. Added configuration for OSX and ARM.
### v2019.0.2
Usage of custom error handlers in connectors. Usage of new app loader. Enable to specify Hub location over CLI.
### v2019.0.1
Added docker image for windows. Updated docker images for linux. Enabled cross-compile. Usage of extended default.config. 
Fixed usage of WUI Connector behind reverse proxy.
### v2019.0.0
Enable to read/write in stream format. Added docker image configuration. Enable reconnect connector in agent mode. 
Fixed creation of shortcuts in embedded mode. Integration of metrics. Fixed bin dependencies for embedded linux executable.
### v2018.3.0
Refactoring into the TypeScript implementation based on WUI Localhost.
### v2018.1.1
Update of XCppCommons and clean up.
### v2018.1.0
Update of XCppCommons. Update of LiveContentProtocol and its propagation into the FFI proxy. Tests clean up.
### v2018.0.3
Added support for libffi build on ARM. Dependencies 7zip and p7zip are configured as common dependency instead of resource. 
Enhancements of logged informations. Added file system expand method to public API.
### v2018.0.2
Fixed issue with service port written to connector.config.jsonp. Invalid value for host-port turns host server to be opened on 
auto-selected port from predefined ones.
Added environment variable expand method to connector public API.
### v2018.0.1
Integration of response API to FFI proxy (renamed from DLL proxy). Changed connector.config.json data related to request origin. 
Cleanup of duplicate resource/libs copy, loaded directly from dependency instead. Extended Terminal API with TerminalOptions and 
environment variables symbols expand.
### v2018.0.0
Added external link library accoding to XCppCommons update. Changed version format.
### v2.2.0
Integrated support for Linux and Mac. Added API to get free port through NetworkingConnector. Fixed some minor issues.
### v2.1.1
Internal configuration file formats migrated from XML to JSONP. Minor cleanup.
### v2.1.0
Added WebResponse and LiveContentResponse and changed old protocols implementation. All platform dependent features ported also on linux 
except Dll/Com-Proxy which are currently excluded. Added regression tests for HttpResolver.
### v2.0.3
Improvement of network computers listing. Increased performance of volume name reader. Documentation update. Bug fix of terminal initialization.
### v2.0.2
DirectoryBrowser back-end enhancements. Firewall exclusion bug fix.
### v2.0.1
Syntax update. Added support for COM and DLL proxy. Update and bug fixes for Terminal and FileSystemHandler.
### v2.0.0
Namespaces refactoring.
### v1.2.0
Refactoring to C++ language
### v1.1.1
Added WindowHandler and performance bug fixes
### v1.1.0
Change of license from proprietary to BSD-3-Clause
### v1.0.0
Initial release

## License

This software is owned or controlled by Oidis.
Use of this software is governed by the BSD-3-Clause License distributed with this material.
 
See the `LICENSE.txt` file distributed for more details.

---

Copyright 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright 2017-2019 [NXP](http://nxp.com/),
Copyright 2019-2025 [Oidis](https://www.oidis.io/)
