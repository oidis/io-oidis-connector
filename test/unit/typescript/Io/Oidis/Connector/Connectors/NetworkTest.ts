/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Network } from "../../../../../../../source/typescript/Io/Oidis/Connector/Connectors/Network.js";

export class NetworkTest extends UnitTestRunner {

    public testIsProxyRequired() : IUnitTestRunnerPromise {
        const network : Network = new Network();
        return ($done : () => void) : void => {
            network.IsProxyRequired(($status : boolean) : void => {
                assert.equal($status, false);
                $done();
            });
        };
    }
}
