/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import {
    IWindowHandlerFileDialogSettings,
    IWindowHandlerSaveFileDialogSettings
} from "@io-oidis-services/Io/Oidis/Services/Connectors/WindowHandlerConnector.js";
import { Dialogs } from "../../../../../../../source/typescript/Io/Oidis/Connector/Connectors/Dialogs.js";

export class DialogsTest extends UnitTestRunner {

    constructor() {
        super();
        this.setMethodFilter("ignoreAll"); // THIS class is only for R&D purposes, asserts can't be automated
        // this.setMethodFilter("testShowSaveFileDialog");
    }

    public testShowFileDialog() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({title: ""});
    }

    public testShowFileDialogTitle() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({title: "Test title"});
    }

    public testShowFileDialogInitFolder() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            initialDirectory: this.getAbsoluteRoot(),
            title           : "Test root"
        });
    }

    public testShowFileDialogOpenOnly() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            openOnly: true,
            title   : "Test open"
        });
    }

    public testShowFileDialogMultiselect() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            multiSelect: true,
            title      : "Test multiselect"
        });
    }

    public testShowFileDialogPath() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            path : this.getAbsoluteRoot() + "/resource/data/Io/Oidis/Connector/Connectors/FileDialog.ps1",
            title: "Test path"
        });
    }

    public testShowFileDialogFileName() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            initialDirectory: this.getAbsoluteRoot(),
            path            : "SomeFile.ext",
            title           : "Test path"
        });
    }

    public testShowFileDialogFilter() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            filter: ["TEXT Files|*.txt;*.csv", "JSON Files|*.json"],
            title : "Test filter"
        });
    }

    public testShowFileDialogFilterIndex() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            filter     : ["Filter 1|*.txt", "Filter 2|*.doc", "Filter 3|.json"],
            filterIndex: 2,
            title      : "Test filter"
        });
    }

    public testShowFolderDialog() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            folderOnly: true,
            title     : ""
        });
    }

    public testShowFolderDialogTitle() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            folderOnly: true,
            title     : "Test title"
        });
    }

    public testShowFolderDialogOpenOnly() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            folderOnly: true,
            openOnly  : true,
            title     : "Test open only"
        });
    }

    public testShowFolderDialogInitFolder() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            folderOnly      : true,
            initialDirectory: this.getAbsoluteRoot(),
            title           : "Test root"
        });
    }

    public testShowFolderDialogPath() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            folderOnly: true,
            path      : this.getAbsoluteRoot() + "/resource/data/Io/Oidis/Connector/Connectors/FileDialog.ps1",
            title     : "Test path"
        });
    }

    public testShowFolderDialogFileName() : IUnitTestRunnerPromise {
        return this.invokeFileDialog({
            folderOnly      : true,
            initialDirectory: this.getAbsoluteRoot(),
            path            : "SomeFile.ext",
            title           : "Test file name"
        });
    }

    public testShowFileExplorer() : IUnitTestRunnerPromise {
        return this.invokeFileExplorer(this.getAbsoluteRoot());
    }

    public testShowFileExplorerCwd() : IUnitTestRunnerPromise {
        return this.invokeFileExplorer("");
    }

    public testShowSaveFileDialog() : IUnitTestRunnerPromise {
        return this.invokeSaveFileDialog({
            autoExtension   : true,
            extension       : "txt",
            filter          : ["Filter 1|*.txt", "Filter 2|*.doc", "Filter 3|.json"],
            filterIndex     : 0,
            initialDirectory: this.getAbsoluteRoot(),
            override        : true,
            path            : "SomeFile.txt",
            restore         : true,
            title           : "Test file name"
        });
    }

    private invokeFileDialog($settings : IWindowHandlerFileDialogSettings) : IUnitTestRunnerPromise {
        this.timeoutLimit(25000);
        const dialogs : Dialogs = new Dialogs();
        return ($done : () => void) : void => {
            dialogs.ShowFileDialogAsync($settings)
                .then(($path : string) : void => {
                    LogIt.Debug($path);
                    assert.notEqual($path, null);
                    $done();
                })
                .catch(($error : Error) : void => {
                    LogIt.Debug($error.message);
                    assert.ok(false);
                });
        };
    }

    private invokeFileExplorer($path : string) : IUnitTestRunnerPromise {
        const dialogs : Dialogs = new Dialogs();
        return ($done : () => void) : void => {
            dialogs.ShowFileExplorerAsync($path)
                .then(() : void => {
                    assert.ok(true);
                    $done();
                })
                .catch(($error : Error) : void => {
                    LogIt.Debug($error.message);
                    assert.ok(false);
                });
        };
    }

    private invokeSaveFileDialog($settings : IWindowHandlerSaveFileDialogSettings) : IUnitTestRunnerPromise {
        this.timeoutLimit(25000);
        const dialogs : Dialogs = new Dialogs();
        return ($done : () => void) : void => {
            dialogs.ShowSaveFileDialogAsync($settings)
                .then(($path : string) : void => {
                    LogIt.Debug($path);
                    assert.notEqual($path, null);
                    $done();
                })
                .catch(($error : Error) : void => {
                    LogIt.Debug($error.message);
                    assert.ok(false);
                });
        };
    }
}
