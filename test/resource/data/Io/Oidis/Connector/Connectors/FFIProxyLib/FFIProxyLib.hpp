/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef FFIPROXYLIB_HPP_
#define FFIPROXYLIB_HPP_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN_PLATFORM
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define DEBUG_OUTPUT(f_, ...)
//#define DEBUG_OUTPUT(f_, ...) printf((f_), ##__VA_ARGS__)

struct TestStruct {
    int a;
    bool b;
    char *c;
};

typedef void (*EmptyCallback)();

typedef int (*IntBoolCallback)(bool);

typedef int (*IntBoolIntStringCallback)(bool, int, char *);

// Test return types
EXPORT int ReturnInt();

EXPORT bool ReturnBool();

EXPORT float ReturnFloat();

EXPORT double ReturnDouble();

EXPORT char *ReturnString();

// Test function input arguments
EXPORT bool CallWithEmpty();

EXPORT bool CallWithInt(int i);

EXPORT bool CallWithBool(bool start);

EXPORT bool CallWithFloat(float f);

EXPORT bool CallWithDouble(double d);

EXPORT bool CallWithString(const char *data);

EXPORT bool CallWithStruct(const TestStruct &data);

EXPORT bool CallWithIntArray(const int *array, int count);

EXPORT bool CallWithEmptyCallback(EmptyCallback callback);

EXPORT int CallWithIntBoolCallback(IntBoolCallback callback);

EXPORT int CallWithIntBoolIntStringCallback(IntBoolIntStringCallback callback);

EXPORT bool CallWithBuffer(const char *buffer, int count);

// Test output arguments
EXPORT void output_args_int(int *a);

EXPORT void output_args_bool(bool *b);

EXPORT void output_args_double(double *d);

EXPORT void output_args_float(float *f);

EXPORT void output_args_char(char *a);

EXPORT void output_args_struct(TestStruct *data);

EXPORT void output_args_buffer(char *buffer);

// Various test methods
EXPORT int int_func_bool_int(bool b, int i);

EXPORT int int_func_int_bool(int i, bool b);

EXPORT int add_input_ints(int a, int b);

EXPORT int output_args_int_bool_float(int *i, bool *b, float *f);

EXPORT int input_int_double_output_bool_int_input_string(int inI, double inD, bool *b, int *i, const char *inC);

EXPORT int append_string(const char *str1, const char *str2, char *output);

#ifdef __cplusplus
}
#endif

#endif  // FFIPROXYLIB_HPP_
