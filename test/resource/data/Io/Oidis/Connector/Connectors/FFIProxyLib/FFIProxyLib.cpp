/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <cstdio>
#include <string>
#include <cstring>
#include "FFIProxyLib.hpp"

const char returnedString[] = "returned string";

// Test return types
EXPORT int ReturnInt() {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    return 23;
}

EXPORT bool ReturnBool() {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    return true;
}

EXPORT float ReturnFloat() {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    return 16.3f;
}

EXPORT double ReturnDouble() {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    return 33.44;
}

EXPORT char *ReturnString() {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    return const_cast<char *>(returnedString);
}

// Test function input arguments
EXPORT bool CallWithEmpty() {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    return true;
}

EXPORT bool CallWithInt(int i) {
    DEBUG_OUTPUT("DLL: %s invoked with: %d\n", __FUNCTION__, i);
    return i == 123;
}

EXPORT bool CallWithBool(bool start) {
    DEBUG_OUTPUT("DLL: %s invoked with: %s\n", __FUNCTION__, start ? "true" : "false");
    return start;
}

EXPORT bool CallWithFloat(float f) {
    DEBUG_OUTPUT("DLL: %s invoked with: %.3f\n", __FUNCTION__, f);
    return f == 3.14f;
}

EXPORT bool CallWithDouble(double d) {
    DEBUG_OUTPUT("DLL: %s invoked with: %.3f\n", __FUNCTION__, d);
    return d == 6.28;
}

EXPORT bool CallWithString(const char *data) {
    DEBUG_OUTPUT("DLL: %s invoked with const char* (CString): %s\n", __FUNCTION__, data);
    return strcmp(data, "hello world") == 0;
}

EXPORT bool CallWithStruct(const TestStruct &data) {
    DEBUG_OUTPUT("DLL: %s invoked with: {a: %d, b: %s, c: %s}\n", __FUNCTION__, data.a, data.b ? "true" : "false", data.c);
    return data.a == 234 && data.b && strcmp(data.c, "struct string") == 0;
}

EXPORT bool CallWithIntArray(const int *array, int count) {
    DEBUG_OUTPUT("DLL: %s invoked with [].length: %d\n", __FUNCTION__, count);
    int sum = 0;
    for (int i = 0; i < count; i++) {
        DEBUG_OUTPUT("%d, ", array[i]);
        sum += array[i];
    }
    return count == 10 && sum == 5145;
}

EXPORT bool CallWithEmptyCallback(EmptyCallback callback) {
    DEBUG_OUTPUT("DLL: %s invoked with EmptyCallback\n", __FUNCTION__);
    callback();
    return true;
}

EXPORT int CallWithIntBoolCallback(IntBoolCallback callback) {
    DEBUG_OUTPUT("DLL: %s invoked with IntBoolCallback\n", __FUNCTION__);
    int retVal = callback(true);
    DEBUG_OUTPUT(">>> callback returns: %d\n", retVal);
    return retVal;
}

EXPORT int CallWithIntBoolIntStringCallback(IntBoolIntStringCallback callback) {
    DEBUG_OUTPUT("DLL: %s invoked with IntBoolIntStringCallback\n", __FUNCTION__);
    int retVal = callback(true, 666, const_cast<char *>("callback string"));
    DEBUG_OUTPUT(">>> callback returns: %d\n", retVal);
    return retVal;
}

EXPORT bool CallWithBuffer(const char *buffer, int count){
    DEBUG_OUTPUT("DLL: %s invoked with [].length: %d\n", __FUNCTION__, count);
    int sum = 0;
    for (int i = 0; i < count; i++) {
        DEBUG_OUTPUT("%d, ", buffer[i]);
        sum += buffer[i];
    }
    return count == 8 && sum == 28;
}

// Test output arguments
EXPORT void output_args_int(int *a) {
    int b = *a;
    DEBUG_OUTPUT("DLL: %s invoked with: %d\n", __FUNCTION__, b);
    *a = 15;
}

EXPORT void output_args_bool(bool *b) {
    DEBUG_OUTPUT("DLL: %s invoked with: %s\n", __FUNCTION__, *b ? "true" : "false");
    *b = false;
}

EXPORT void output_args_double(double *d) {
    DEBUG_OUTPUT("DLL: %s invoked with: %.3f\n", __FUNCTION__, *d);
    *d = 56.12f;
}

EXPORT void output_args_float(float *f) {
    DEBUG_OUTPUT("DLL: %s invoked with: %.3f\n", __FUNCTION__, *f);
    *f = 12.12f;
}

EXPORT void output_args_char(char *a) {
    DEBUG_OUTPUT("DLL: %s invoked with: %s\n", __FUNCTION__, a);

    a[0] = 's';
    a[1] = 'o';
    a[2] = 'm';
    a[3] = 'e';
    a[4] = ' ';
    a[5] = 't';
    a[6] = 'e';
    a[7] = 'x';
    a[8] = 't';
    a[9] = '\0';
}

EXPORT void output_args_struct(TestStruct *data) {
    DEBUG_OUTPUT("DLL: %s invoked\n", __FUNCTION__);
    data->a = 456;
    data->b = false;
    data->c = const_cast<char *>("new struct string");
}

EXPORT void output_args_buffer(char *buffer) {
    DEBUG_OUTPUT("DLL: %s invoked, ptr: %s\n", __FUNCTION__, (buffer == nullptr) ? "null" : "valid");
    if (buffer != nullptr) {
        for (int i = 0; i < 20; i++) {
            buffer[i] = i;
        }
    }
}


// Various test methods
EXPORT int int_func_bool_int(bool b, int i) {
    DEBUG_OUTPUT("DLL: %s invoked with: %s and %d\n", __FUNCTION__, b ? "true" : "false", i);
    return 23;
}

EXPORT int int_func_int_bool(int i, bool b) {
    DEBUG_OUTPUT("DLL: %s invoked with: %d and %s\n", __FUNCTION__, i, b ? "true" : "false");
    return 29;
}

EXPORT int add_input_ints(int a, int b) {
    DEBUG_OUTPUT("DLL: %s invoked with: %d and %d\n", __FUNCTION__, a, b);
    return a + b;
}

EXPORT int output_args_int_bool_float(int *i, bool *b, float *f) {
    DEBUG_OUTPUT("DLL: %s invoked with: %d, %s, and %.3f\n", __FUNCTION__, *i, *b ? "true" : "false", *f);
    *i = 12;
    *b = false;
    *f = 3.14;
    return 3;
}

EXPORT int input_int_double_output_bool_int_input_string(int inI, double inD, bool *b, int *i, const char *inC) {
    DEBUG_OUTPUT("DLL: %s invoked with: %d, %.3f, %s, %d, %s\n", __FUNCTION__, inI, inD, *b ? "true" : "false", *i, inC);
    *b = false;
    *i = 432;
    return 8;
}

EXPORT int append_string(const char *str1, const char *str2, char *output) {
    DEBUG_OUTPUT("DLL: %s invoked with: %s, %s, %s\n", __FUNCTION__, str1, str2, output);
    std::string tmp(str1);
    tmp += str2;
    memcpy(output, tmp.c_str(), tmp.length());
    DEBUG_OUTPUT(">>>: %s", output);
    return tmp.length();
}
