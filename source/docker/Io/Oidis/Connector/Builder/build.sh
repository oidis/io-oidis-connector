#!/usr/bin/env bash
# * ********************************************************************************************************* *
# *
# * Copyright 2023-2024 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

profile="${PROFILE:-eap}"

oidis --version

oidis install --solution=shared || exit 1

# hotfix for missing modules before test build
oidis rebuild-dev --solution=shared || exit 1

oidis test coverage --solution=shared || exit 1

oidis rebuild-eap --solution=shared || exit 1

#./build/target/cmd/OidisConnector.sh --integrity-check || exit 1

if [[ -z "${DEPLOY_DISABLED}" ]]; then
  if [[ $profile == "prod" ]]; then
    oidis target-deploy:prod --solution=shared || exit 1
  else
    oidis target-deploy:eap --solution=shared || exit 1
    oidis target-deploy:dev --solution=shared || exit 1
  fi
fi
