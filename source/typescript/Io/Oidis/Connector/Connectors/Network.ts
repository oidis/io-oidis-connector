/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import getPort from "@nodejs/get-port/index.js";
import { Loader } from "../Loader.js";

export class Network extends BaseConnector {
    private static agents : ArrayList<any>;
    private static callbacksByPort : ArrayList<ArrayList<any>>;

    @Extern()
    public getFreePort($callback : (($value : number) => void) | IResponse) : void {
        getPort().then(($port) : void => {
            ResponseFactory.getResponse($callback).Send($port);
        });
    }

    public IsPortFree($value : number, $callback : (($status : boolean) => void) | IResponse) : void {
        const isPortFree : any = require("is-port-free");
        isPortFree($value)
            .then(() : void => {
                ResponseFactory.getResponse($callback).Send(true);
            })
            .catch(() : void => {
                ResponseFactory.getResponse($callback).Send(false);
            });
    }

    public IsProxyRequired($callback : (($status : boolean) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        Loader.getInstance().getHttpManager().IsOnline(($status : boolean) : void => {
            if ($status) {
                Loader.getInstance().getFileSystemHandler().Download({
                    streamOutput: true,
                    url         : "https://hub.oidis.io/ping.txt"
                }, ($headers : string, $body : string) : void => {
                    if (StringUtils.IsEmpty($body)) {
                        response.Send(false);
                    } else {
                        let httpProxy : string = Loader.getInstance().getAppConfiguration().httpProxy;
                        if (ObjectValidator.IsEmptyOrNull(httpProxy)) {
                            response.Send(true);
                        } else {
                            if (!StringUtils.StartsWith(httpProxy, "http://")) {
                                httpProxy = "http://" + httpProxy;
                            }
                            if (!StringUtils.EndsWith(httpProxy, "/")) {
                                httpProxy += "/";
                            }
                            const retVal : any = require("child_process").spawnSync(
                                "ping " + require("url").parse(httpProxy).hostname + " -n 1", {
                                    shell      : true,
                                    windowsHide: false
                                });
                            response.Send(retVal.status !== 0);
                        }
                    }
                });
            } else {
                response.Send(false);
            }
        });
    }

    @Extern()
    public AddPortListener($port : number, $callback : IResponse) : void {
        this.initCache($port);
        const response : IResponse = ResponseFactory.getResponse($callback);
        const sessionId : string = this.initSession(response, $port);
        if (!this.isPortAgentSet($port)) {
            this.createPortListener($port, sessionId, () : void => {
                response.OnStart(sessionId);
            });
        } else {
            response.OnStart(sessionId);
        }
    }

    @Extern()
    public RemovePortListener($sessionId : string, $port : number) : void {
        this.initCache($port);
        const callbacks : ArrayList<any> = Network.callbacksByPort.getItem($port);
        const agent : any = Network.agents.getItem($port);
        if (!(ObjectValidator.IsEmptyOrNull(agent) && (
            ObjectValidator.IsEmptyOrNull($sessionId) || (callbacks.Contains($sessionId) && callbacks.Length() === 1)))) {
            Network.agents.getItem($port).close();
        } else {
            Network.callbacksByPort.RemoveAt(Network.callbacksByPort.IndexOf(callbacks));
        }
    }

    @Extern()
    public WriteToPort($data : string, $port : number, $callback? : IResponse) : void {
        this.initCache($port);
        const response : IResponse = ResponseFactory.getResponse($callback);
        if (this.isPortAgentSet($port)) {
            Network.agents.getItem($port).write(Buffer.from($data, "base64"));
            response.Send(0);
        } else {
            response.OnError("Port listener is not initialized.");
        }
    }

    private initSession($callback : any, $port : number) : string {
        const sessionId : string = StringUtils.getSha1("" + new Date().getTime() + $port + Math.random());
        Network.callbacksByPort.getItem($port).Add($callback, sessionId);
        return sessionId;
    }

    private createPortListener($port : number, $sessionId : string, $callback : any) : void {
        const net = require("net");
        const serviceAgent : any = new net.Socket();
        serviceAgent.setTimeout(15 * 60 * 1000);
        serviceAgent.connect($port, "localhost");
        serviceAgent.on("connect", () => {
            // LogIt.Debug("connected");
            Network.agents.Add(serviceAgent, $port);
            $callback();
        });

        serviceAgent.on("data", ($data : any) => {
            // LogIt.Debug($data.toString());
            Network.callbacksByPort.getItem($port).foreach(($callback : any) : void => {
                $callback.OnChange($data.toString("base64"));
            });
        });

        serviceAgent.on("error", ($data : any) => {
            // LogIt.Debug($data);
            Network.callbacksByPort.getItem($port).foreach(($callback : any) : void => {
                $callback.OnError($data);
            });
        });

        serviceAgent.on("timeout", ($data : any) => {
            // LogIt.Debug($data);
            serviceAgent.close();
        });

        serviceAgent.on("close", ($data : any) => {
            // LogIt.Debug($data);
            Network.callbacksByPort.getItem($port).foreach(($callback : any) : void => {
                $callback.OnComplete($sessionId);
            });
            Network.callbacksByPort.RemoveAt(Network.callbacksByPort.IndexOf(Network.callbacksByPort.getItem($port)));
            Network.agents.RemoveAt(Network.agents.IndexOf(serviceAgent));
        });
    }

    private isPortAgentSet($port : number) : boolean {
        return !ObjectValidator.IsEmptyOrNull(Network.agents) && !ObjectValidator.IsEmptyOrNull(Network.agents.getItem($port));
    }

    private initCache($port : number) : void {
        if (!ObjectValidator.IsSet(Network.agents)) {
            Network.agents = new ArrayList<any>();
        }
        if (!ObjectValidator.IsSet(Network.callbacksByPort)) {
            Network.callbacksByPort = new ArrayList<any>();
        }
        if (ObjectValidator.IsEmptyOrNull(Network.callbacksByPort.getItem($port))) {
            Network.callbacksByPort.Add(new ArrayList<any>(), $port);
        }
    }
}
