/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ITerminalOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/TerminalConnector.js";
import {
    IWindowHandlerFileDialogSettings,
    IWindowHandlerSaveFileDialogSettings
} from "@io-oidis-services/Io/Oidis/Services/Connectors/WindowHandlerConnector.js";
import { Loader } from "../Loader.js";
import { FileSystemHandler } from "./FileSystemHandler.js";

export class Dialogs extends BaseConnector {

    @Extern()
    public ShowFileDialog($settings : IWindowHandlerFileDialogSettings,
                          $callback : (($status : boolean, $path : string) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        this.ShowFileDialogAsync($settings)
            .then(($path : string) : void => {
                response.Send(true, $path);
            })
            .catch(($error : Error) : void => {
                LogIt.Debug($error.message);
                response.Send(false, null);
            });
    }

    @Extern()
    public ShowSaveFileDialog($settings : IWindowHandlerSaveFileDialogSettings,
                              $callback : (($status : boolean, $path : string) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        this.ShowSaveFileDialogAsync($settings)
            .then(($path : string) : void => {
                response.Send(true, $path);
            })
            .catch(($error : Error) : void => {
                LogIt.Debug($error.message);
                response.Send(false, null);
            });
    }

    @Extern()
    public ShowFileExplorer($path : string, $callback : (($status : boolean) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        this.ShowFileExplorerAsync($path)
            .then(() : void => {
                response.Send(true);
            })
            .catch(($error : Error) : void => {
                LogIt.Debug($error.message);
                response.Send(false);
            });
    }

    public ShowFileDialogAsync($settings : IWindowHandlerFileDialogSettings) : Promise<string> {
        let cmd : string = "";
        let args : string[] = [];
        let cwd : string | ITerminalOptions = null;
        let supported : boolean = true;
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const path : any = require("path");

        $settings = JsonUtils.Extend(<IWindowHandlerFileDialogSettings>{
            filter          : [],
            filterIndex     : 0,
            folderOnly      : false,
            initialDirectory: "",
            multiSelect     : false,
            openOnly        : false,
            path            : "",
            singleton       : false,
            timeout         : -1,
            title           : ""
        }, $settings);

        if (!ObjectValidator.IsEmptyOrNull($settings.initialDirectory)) {
            $settings.initialDirectory = fileSystem.NormalizePath($settings.initialDirectory);
        }
        if (!ObjectValidator.IsEmptyOrNull($settings.path)) {
            $settings.path = fileSystem.NormalizePath($settings.path);
            if (StringUtils.Contains($settings.path, "/")) {
                $settings.initialDirectory = path.dirname($settings.path);
                $settings.path = path.basename($settings.path);
            }
        }

        if ($settings.folderOnly) {
            LogIt.Debug("Open folder dialog");
            if (EnvironmentHelper.IsWindows()) {
                if (StringUtils.Contains($settings.initialDirectory, "/")) {
                    $settings.path = $settings.initialDirectory;
                    $settings.initialDirectory = "";
                }

                cmd = "wscript.exe";
                args = [
                    "powershell.vbs",
                    "\"",
                    ".\\FolderDialog.ps1",
                    "-initDir", "'" + StringUtils.Replace($settings.initialDirectory, "/", "\\") + "'",
                    "-path", "'" + StringUtils.Replace($settings.path, "/", "\\") + "'",
                    "-title", "'" + $settings.title + "'",
                    "-openOnly ", $settings.openOnly ? "$True" : "$False",
                    "\""
                ];
                cwd = {
                    cwd     : Loader.getInstance().getProgramArgs().ProjectBase() + "/resource/libs/Connectors",
                    detached: true
                };
            } else if (EnvironmentHelper.IsMac()) {
                cmd = "osascript";
                args = [
                    "-e", "'POSIX path of (choose folder with prompt \"" + $settings.title + "\" default location \"" +
                    $settings.initialDirectory + "/" + $settings.path + "\" with showing package contents)'"
                ];
            } else if (EnvironmentHelper.IsLinux()) {
                cmd = "zenity";
                args = ["--file-selection", "--directory", "--title", "\"" + $settings.title + "\""];
            } else {
                supported = false;
            }
        } else {
            LogIt.Debug("Open file dialog");
            if (EnvironmentHelper.IsWindows()) {
                cmd = "wscript.exe";
                args = [
                    "powershell.vbs",
                    "\"",
                    ".\\FileDialog.ps1",
                    "-initDir", "'" + StringUtils.Replace($settings.initialDirectory, "/", "\\") + "'",
                    "-path", "'" + StringUtils.Replace($settings.path, "/", "\\") + "'",
                    "-title", "'" + $settings.title + "'",
                    "-filter", "'" + $settings.filter.join("|") + "'",
                    "-filterIndex", $settings.filterIndex + "",
                    "-multiselect ", $settings.multiSelect ? "$True" : "$False",
                    "-openOnly ", $settings.openOnly ? "$True" : "$False",
                    "\""
                ];
                cwd = {
                    cwd     : Loader.getInstance().getProgramArgs().ProjectBase() + "/resource/libs/Connectors",
                    detached: true
                };
            } else if (EnvironmentHelper.IsMac()) {
                cmd = "osascript";
                args = [
                    "-e", "'POSIX path of (choose file with prompt \"" + $settings.title + "\" default location \"" +
                    $settings.initialDirectory + "\")'"
                ];
            } else if (EnvironmentHelper.IsLinux()) {
                cmd = "zenity";
                args = ["--file-selection", "--title", "\"" + $settings.title + "\""];
            } else {
                supported = false;
            }
        }
        if (supported) {
            return this.invokeDialog({
                args,
                cmd,
                cwd,
                failedToInvoke: "Failed to invoke native file dialog browser",
                fileSystem,
                notSelected   : "Folder or file has not been selected",
                scriptPath    : $settings.folderOnly ? ".\\FolderDialog" : ".\\FileDialog.ps1",
                singleton     : $settings.singleton,
                timeout       : $settings.timeout
            });
        } else {
            throw new Error("Unsupported platform for file dialog browser");
        }
    }

    public ShowSaveFileDialogAsync($settings : IWindowHandlerSaveFileDialogSettings) : Promise<string> {
        let cmd : string = "";
        let args : string[] = [];
        let cwd : string | ITerminalOptions = null;
        let supported : boolean = true;
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const path : any = require("path");

        $settings = JsonUtils.Extend(<IWindowHandlerSaveFileDialogSettings>{
            autoExtension   : true,
            extension       : "",
            filter          : [],
            filterIndex     : 0,
            initialDirectory: "",
            override        : false,
            path            : "",
            restore         : true,
            singleton       : false,
            timeout         : -1,
            title           : ""
        }, $settings);

        if (!ObjectValidator.IsEmptyOrNull($settings.initialDirectory)) {
            $settings.initialDirectory = fileSystem.NormalizePath($settings.initialDirectory);
        }
        if (!ObjectValidator.IsEmptyOrNull($settings.path)) {
            $settings.path = fileSystem.NormalizePath($settings.path);
            if (StringUtils.Contains($settings.path, "/")) {
                $settings.initialDirectory = path.dirname($settings.path);
                $settings.path = path.basename($settings.path);
            }
        }

        LogIt.Debug("Open save file dialog");
        if (EnvironmentHelper.IsWindows()) {
            cmd = "wscript.exe";
            args = [
                "powershell.vbs",
                "\"",
                ".\\SaveFileDialog.ps1",
                "-initDir", "'" + StringUtils.Replace($settings.initialDirectory, "/", "\\") + "'",
                "-path", "'" + StringUtils.Replace($settings.path, "/", "\\") + "'",
                "-title", "'" + $settings.title + "'",
                "-filter", "'" + $settings.filter.join("|") + "'",
                "-filterIndex", $settings.filterIndex + "",
                "-extension ", "'" + $settings.extension + "'",
                "-autoExtension ", $settings.autoExtension ? "$True" : "$False",
                "-restore ", $settings.restore ? "$True" : "$False",
                "-override ", $settings.override ? "$True" : "$False",
                "\""
            ];
            cwd = {
                cwd     : Loader.getInstance().getProgramArgs().ProjectBase() + "/resource/libs/Connectors",
                detached: true
            };
        } else {
            supported = false;
        }
        if (supported) {
            return this.invokeDialog({
                args,
                cmd,
                cwd,
                failedToInvoke: "Failed to invoke native save file dialog browser",
                fileSystem,
                notSelected   : "File has not been selected",
                scriptPath    : ".\\SaveFileDialog.ps1",
                singleton     : $settings.singleton,
                timeout       : $settings.timeout
            });
        } else {
            throw new Error("Unsupported platform for save file dialog browser");
        }
    }

    public async ShowFileExplorerAsync($path : string) : Promise<void> {
        return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
            let cmd : string = "";
            let args : string[] = [];
            const cwd : string = null;
            let supported : boolean = true;
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const path : any = require("path");

            if (ObjectValidator.IsEmptyOrNull($path)) {
                $path = ".";
            } else if (!fileSystem.IsDirectory($path)) {
                $path = path.dirname($path);
            }
            $path = fileSystem.NormalizePath($path);

            if (EnvironmentHelper.IsWindows()) {
                $path = StringUtils.Replace($path, "/", "\\");
                cmd = "powershell";
                args = ["ii", "\"" + $path + "\""];
            } else if (EnvironmentHelper.IsMac()) {
                cmd = "open";
                args = ["\"" + $path + "\""];
            } else if (EnvironmentHelper.IsLinux()) {
                cmd = "gnome-open";
                args = ["\"" + $path + "\""];
            } else {
                supported = false;
            }

            if (supported) {
                Loader.getInstance().getTerminal().Spawn(cmd, args, cwd, ($statusCode : number) : void => {
                    if ($statusCode === 0) {
                        $resolve();
                    } else {
                        $reject(new Error("Failed to invoke native file explorer window"));
                    }
                });
            } else {
                $reject(new Error("Unsupported platform for native file explorer window"));
            }
        });
    }

    private async invokeDialog($options : any) : Promise<string> {
        if ($options.singleton) {
            await this.killDialog($options.scriptPath);
        }
        let timeoutId : any;
        if ($options.timeout > 0) {
            timeoutId = setTimeout(async () : Promise<void> => {
                await this.killDialog($options.scriptPath);
            }, $options.timeout);
        }
        const result : IExecuteResult = await Loader.getInstance().getTerminal().SpawnAsync($options.cmd, $options.args, $options.cwd);
        if (!ObjectValidator.IsEmptyOrNull(timeoutId)) {
            clearTimeout(timeoutId);
        }
        let path : string = null;
        if (result.exitCode === 0) {
            path = result.std[0].trim();
            path = StringUtils.Replace(path, "\r\n", "\n");
            path = StringUtils.Split(path, "\n").map(($item) : string => {
                return $options.fileSystem.NormalizePath($item);
            }).join(":");

            if (ObjectValidator.IsEmptyOrNull(path) || path === ".") {
                throw new Error($options.notSelected);
            } else {
                return path;
            }
        } else {
            throw new Error($options.failedToInvoke);
        }
    }

    private async killDialog($script : string) : Promise<boolean> {
        if (EnvironmentHelper.IsWindows()) {
            const result : IExecuteResult = await Loader.getInstance().getTerminal().ExecuteAsync("wmic", [
                "process", "where name=\"powershell.exe\" get commandline, processid"
            ], {
                verbose: false
            });
            if (result.exitCode === 0) {
                const outList : string = (<any>result.std[0]).trim();
                if (!ObjectValidator.IsEmptyOrNull(outList)) {
                    const entries : string[] = StringUtils.Split(StringUtils.Remove(outList, "\r"), "\n");
                    let status : boolean = true;
                    entries.forEach(($item : any) : void => {
                        if (StringUtils.Contains($item, "-executionpolicy bypass -Command  " + $script)) {
                            const parts : any[] = $item.trim().replace(/\s+/g, "*").split("*");
                            if (parts.length >= 2) {
                                const pid : number = StringUtils.ToInteger(parts[parts.length - 1]);
                                if (pid !== process.pid) {
                                    try {
                                        process.kill(pid);
                                    } catch (ex) {
                                        status = false;
                                        LogIt.Warning("Failed to kill process with PID " + pid + ": " + ex.message);
                                    }
                                }
                            }
                        }
                    });
                    return status;
                } else {
                    return true;
                }
            } else {
                LogIt.Warning(result.std[0] + result.std[1]);
                return false;
            }
        }
        LogIt.Warning("Dialog singleton mode is supported only on Windows platform");
        return false;
    }
}
