/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { AgentsRegisterConnector, IAgentCapabilities } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { IAcknowledgePromise } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { ILiveContentErrorPromise } from "@io-oidis-services/Io/Oidis/Services/Interfaces/ILiveContentPromise.js";
import { ILiveContentProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { IConnectorOptions } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { IAuthorizedMethods } from "@io-oidis-services/Io/Oidis/Services/Structures/IAuthorizedMethods.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { IProject } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";

export class HubConnector extends AgentsRegisterConnector {

    constructor($reconnect : boolean | number = true, $serverConfigurationSource? : string | WebServiceConfiguration) {
        super($reconnect, $serverConfigurationSource);
    }

    public RegisterAgent($capabilities : IAgentCapabilities) : IConnectorHubRegisterPromise {
        const callbacks : any = {
            onComplete($success : boolean, $authMethods : IAuthorizedMethods[]) : any {
                // declare default callback
            },
            onError: null,
            onMessage($data : ILiveContentProtocol, $taskId : string) : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
            "RegisterAgent", $capabilities)
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_CHANGE) {
                        const data : IConnectorRequestProtocol = $result.data;
                        callbacks.onMessage(data.protocol, data.taskId);
                    }
                } else if (ObjectValidator.IsBoolean($result)) {
                    callbacks.onComplete($result, []);
                } else if (ObjectValidator.IsArray($result) && $result.length === 2) {
                    callbacks.onComplete($result[0], $result[1]);
                }
            });
        const promise : IConnectorHubRegisterPromise = {
            OnError  : ($callback : ($args : ErrorEventArgs) => void) : IAcknowledgePromise => {
                callbacks.onError = $callback;
                return promise;
            },
            OnMessage: ($callback : ($data : ILiveContentProtocol, $taskId : string) => void) : IRegisterPromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then     : ($callback : ($success : boolean, $authMethods : IAuthorizedMethods[]) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public InitPipe($pipeId : string) : IConnectorHubPipeSubscribePromise {
        const callbacks : any = {
            onConnect($clientId : string) : any {
                // declare default callback
            },
            onMessage($data : string) : any {
                // declare default callback
            },
            onDisconnect() : any {
                // declare default callback
            },
            onComplete($status : boolean, $message? : string) : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
            "InitPipe", $pipeId)
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_START) {
                        callbacks.onConnect($result.data);
                    } else if ($result.type === EventType.ON_CHANGE) {
                        callbacks.onMessage($result.data);
                    } else if ($result.type === EventType.ON_COMPLETE) {
                        callbacks.onDisconnect();
                    }
                } else {
                    callbacks.onComplete($result[0], $result[1]);
                }
            });

        const promise : IConnectorHubPipeSubscribePromise = {
            OnConnect   : ($callback : ($clientId : string) => void) : IConnectorHubPipeSubscribePromise => {
                callbacks.onConnect = $callback;
                return promise;
            },
            OnDisconnect: ($callback : () => void) : IConnectorHubPipeSubscribePromise => {
                callbacks.onDisconnect = $callback;
                return promise;
            },
            OnMessage   : ($callback : ($protocol : IConnectorPipeProtocol) => void) : IConnectorHubPipeSubscribePromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then        : ($callback : ($status : boolean, $message? : string) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public WriteToPipe($sourceClientId : string, $pipeId : string,
                       $protocol : IConnectorPipeProtocol) : IConnectorHubPipeWritePromise {
        const callbacks : any = {
            onComplete($status : boolean, $message? : string) : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
            "WriteToPipe", $sourceClientId, $pipeId, $protocol)
            .Then(($result : any) : void => {
                callbacks.onComplete($result[0], $result[1]);
            });
        return {
            Then: ($callback : ($status : boolean, $message? : string) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
    }

    public UpdateAgentStatus($status : any) : Promise<any> {
        return this.asyncInvoke("UpdateAgentStatus", $status);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.ConnectorHub";
        return namespaces;
    }

    protected resolveDefaultOptions($options : IConnectorOptions) : IConnectorOptions {
        if (ObjectValidator.IsEmptyOrNull($options.serverConfigurationSource)) {
            const appConf : IProject = Loader.getInstance().getAppConfiguration();
            if (appConf.hubLocations.hasOwnProperty(appConf.agent.hub)) {
                $options.serverConfigurationSource = appConf.hubLocations[appConf.agent.hub] + "/connector.config.jsonp";
            } else {
                $options.serverConfigurationSource = appConf.agent.hub + "/connector.config.jsonp";
            }
        }
        return super.resolveDefaultOptions($options);
    }
}

export interface IConnectorRequestProtocol {
    protocol : ILiveContentProtocol;
    taskId : string;
}

export interface IRegisterPromise extends ILiveContentErrorPromise {
    Then($callback : ($success : boolean, $authMethods : IAuthorizedMethods[]) => void) : void;
}

export interface IStatusUpdatePromise extends ILiveContentErrorPromise {
    Then($callback : ($data : any) => void) : void;
}

export interface IConnectorHubRegisterPromise extends IRegisterPromise {
    OnMessage($callback : ($data : ILiveContentProtocol, $taskId : string) => void) : IRegisterPromise;
}

export interface IConnectorHubPipeSubscribePromise {
    OnMessage($callback : ($data : IConnectorPipeProtocol) => void) : IConnectorHubPipeSubscribePromise;

    OnConnect($callback : ($clientId : string) => void) : IConnectorHubPipeSubscribePromise;

    OnDisconnect($callback : () => void) : IConnectorHubPipeSubscribePromise;

    Then($callback : ($status : boolean, $message? : string) => void) : void;
}

export interface IConnectorHubPipeWritePromise {
    Then($callback : ($status : boolean, $message? : string) => void) : void;
}

export interface IConnectorPipeProtocol {
    type : string;
    data? : any;
}

// generated-code-start
export const IConnectorRequestProtocol = globalThis.RegisterInterface(["protocol", "taskId"]);
export const IRegisterPromise = globalThis.RegisterInterface(["Then"], <any>ILiveContentErrorPromise);
export const IStatusUpdatePromise = globalThis.RegisterInterface(["Then"], <any>ILiveContentErrorPromise);
export const IConnectorHubRegisterPromise = globalThis.RegisterInterface(["OnMessage"], <any>IRegisterPromise);
export const IConnectorHubPipeSubscribePromise = globalThis.RegisterInterface(["OnMessage", "OnConnect", "OnDisconnect", "Then"]);
export const IConnectorHubPipeWritePromise = globalThis.RegisterInterface(["Then"]);
export const IConnectorPipeProtocol = globalThis.RegisterInterface(["type", "data"]);
// generated-code-end
