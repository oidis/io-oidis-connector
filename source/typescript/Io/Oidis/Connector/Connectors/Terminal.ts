/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Terminal as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { ITerminalOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/TerminalConnector.js";

export class Terminal extends Parent {

    @Extern(false)
    public Execute($cmd : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                   $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
        return super.Execute($cmd, $args, $cwdOrOptions, $callback);
    }

    @Extern(false)
    public Spawn($cmd : string, $args : string[], $cwdOrOptions : string | ITerminalOptions,
                 $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
        return super.Spawn($cmd, $args, $cwdOrOptions, $callback);
    }

    @Extern(false)
    public Open($path : string, $callback? : (($exitCode : number) => void) | IResponse) : number {
        return super.Open($path, $callback);
    }

    @Extern(false)
    public Elevate($cmd : string, $args : string[], $cwd : string,
                   $callback? : (($exitCode : number, $std? : string[]) => void) | IResponse) : number {
        return super.Elevate($cmd, $args, $cwd, $callback);
    }

    @Extern(false)
    public Kill($pathOrName : string, $callback? : (($status : boolean) => void) | IResponse) : void {
        return super.Kill($pathOrName, $callback);
    }
}
