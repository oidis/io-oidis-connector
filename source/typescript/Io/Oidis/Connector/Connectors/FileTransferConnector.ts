/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { IFileTransferCallbacks } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileTransferHandler.js";
import { IFileInfo } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IFileInfo.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";
import { FileTransferHandler } from "./FileTransferHandler.js";

export class FileTransferConnector extends BaseConnector {

    public static getUploadProtocol($connector : BaseConnector, $methodName : string, $fileInfo : IFileInfo,
                                    $sourcePath : string) : IFileTransferPromise {
        const callbacks : IFileTransferCallbacks = {
            sendData: ($data : IFileTransferProtocol, $callback : ($success : boolean) => void) : void => {
                (<any>$connector).invoke($methodName, $data).Then($callback);
            },
            onMessage($data : string) : any {
                // declare default callback
            },
            onComplete($chunkId : string) : any {
                // declare default callback
            },
            onError($chunkId : string, $data : string) : any {
                // declare default callback
            }
        };
        FileTransferHandler.TransferFile($fileInfo, $sourcePath, callbacks);
        const promise : IFileTransferPromise = {
            OnError  : ($callback : ($data : string) => void) : IFileTransferPromise => {
                callbacks.onError = ($chunkId : string, $data : string) : void => {
                    $callback($data);
                };
                return promise;
            },
            OnMessage: ($callback : ($data : string) => void) : IFileTransferPromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then     : ($callback : () => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public DownloadFile($fileInfo : IFileInfo, $targetPath : string) : IFileTransferPromise {
        const callbacks : any = {
            onMessage($data : string) : any {
                // declare default callback
            },
            onComplete() : any {
                // declare default callback
            },
            onError($data : string) : any {
                // declare default callback
            }
        };
        const promise : IFileTransferPromise = {
            OnError  : ($callback : ($data : string) => void) : IFileTransferPromise => {
                callbacks.onError = $callback;
                return promise;
            },
            OnMessage: ($callback : ($data : string) => void) : IFileTransferPromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            Then     : ($callback : () => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        this.invoke("DownloadFile", $fileInfo)
            .Then(($result : any) : void => {
                if (ObjectValidator.IsEmptyOrNull($result.type)) {
                    let data : any = $result.data;
                    if (ObjectValidator.IsString(data)) {
                        data = <any>Buffer.from(data, "base64");
                    }
                    Loader.getInstance().getFileSystemHandler().Write($targetPath, data, $result.index !== 0);
                    if ($result.end > $result.size) {
                        this.invoke("AcknowledgeChunk", $result.id);
                    }
                    callbacks.onMessage(FileTransferHandler.getTransferMessage(data));
                } else if ($result.type === EventType.ON_COMPLETE) {
                    callbacks.onComplete();
                } else if ($result.type === EventType.ON_ERROR) {
                    callbacks.onError($result.data);
                }
            });
        return promise;
    }

    public UploadFile($fileInfo : IFileInfo, $sourcePath : string) : IFileTransferPromise {
        return FileTransferConnector.getUploadProtocol(this, "UploadFile", $fileInfo, $sourcePath);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Connector.Connectors.FileTransferHandler";
        namespaces[WebServiceClientType.FORWARD_CLIENT] = "Io.Oidis.Connector.Connectors.FileTransferHandler";
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Connectors.FileTransferHandler";
        return namespaces;
    }
}

export interface IFileTransferPromise {
    OnMessage($callback : ($data : string) => void) : IFileTransferPromise;

    OnError($callback : ($data : string) => void) : IFileTransferPromise;

    Then($callback : () => void) : void;
}

// generated-code-start
export const IFileTransferPromise = globalThis.RegisterInterface(["OnMessage", "OnError", "Then"]);
// generated-code-end
