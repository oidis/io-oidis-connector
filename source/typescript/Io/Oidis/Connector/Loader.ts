/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/EventsManager.js";
import { WebResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/WebResponse.js";
import { IRequestConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IRequestConnector.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Loader as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Loader.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { Metrics } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/Metrics.js";
import { AgentsRegisterConnector, IAgentMetadata } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { IAuthorizedMethods } from "@io-oidis-services/Io/Oidis/Services/Structures/IAuthorizedMethods.js";
import { FileSystemHandler } from "./Connectors/FileSystemHandler.js";
import { HubConnector } from "./Connectors/HubConnector.js";
import { Terminal } from "./Connectors/Terminal.js";
import { HttpResolver } from "./HttpProcessor/HttpResolver.js";
import { HttpServer } from "./HttpProcessor/HttpServer.js";
import { IProject } from "./Interfaces/IProject.js";
import { HeadlessSnippet } from "./RuntimeTests/HeadlessSnippet.js";
import { ProgramArgs } from "./Structures/ProgramArgs.js";

export class Loader extends Parent {
    private loopThread : any;

    public static getInstance() : Loader {
        return <Loader>super.getInstance();
    }

    public getAppConfiguration() : IProject {
        return <IProject>super.getAppConfiguration();
    }

    public getHttpResolver() : HttpResolver {
        return <HttpResolver>super.getHttpResolver();
    }

    public getProgramArgs() : ProgramArgs {
        return <ProgramArgs>super.getProgramArgs();
    }

    public getFileSystemHandler() : FileSystemHandler {
        return <FileSystemHandler>super.getFileSystemHandler();
    }

    public getTerminal() : Terminal {
        return <Terminal>super.getTerminal();
    }

    protected initProgramArgs() : ProgramArgs {
        return new ProgramArgs();
    }

    protected initHttpServer() : HttpServer {
        return new HttpServer();
    }

    protected getHttpServer() : HttpServer {
        return <HttpServer>super.getHttpServer();
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver("/");
    }

    protected initFileSystem() : FileSystemHandler {
        return new FileSystemHandler();
    }

    protected initTerminal() : Terminal {
        return new Terminal();
    }

    protected initAgentConnector() : AgentsRegisterConnector {
        return new HubConnector(10);
    }

    protected processProgramArgs() : boolean {
        const args : ProgramArgs = this.getProgramArgs();
        if (args.IsAgent()) {
            Metrics.getInstance().Instrument([
                globalThis.Io.Oidis.Connector.Connectors
            ]);
            args.StartServer(false);
            super.processProgramArgs();
            args.TargetBase(args.ProjectBase());

            if (!ObjectValidator.IsEmptyOrNull(args.HubLocation())) {
                this.getAppConfiguration().agent.hub = args.HubLocation();
            }
            const hubLocation : string = StringUtils.ToLowerCase(this.getAppConfiguration().agent.hub);
            if (this.getAppConfiguration().hubLocations.hasOwnProperty(hubLocation)) {
                this.getAppConfiguration().agent.hub = this.getAppConfiguration().hubLocations[hubLocation];
            }
            if (ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().agent.credentials)) {
                this.getAppConfiguration().agent.credentials = {authToken: "", userName: ""};
            }
            if (!ObjectValidator.IsEmptyOrNull(args.UserName())) {
                this.getAppConfiguration().agent.credentials.userName = args.UserName();
            }
            if (!ObjectValidator.IsEmptyOrNull(args.Token())) {
                this.getAppConfiguration().agent.credentials.authToken = args.Token();
            }
            if (!ObjectValidator.IsEmptyOrNull(args.Groups())) {
                this.getAppConfiguration().agent.credentials.groups = args.Groups();
            }

            let configuration : any = {};
            if (!ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().agent.configuration)) {
                configuration = this.getAppConfiguration().agent.configuration;
            }

            const connector : HubConnector = <HubConnector>this.initAgentConnector();
            let platform : string = "unrecognized";
            if (EnvironmentHelper.IsWindows()) {
                platform = "win";
            } else if (EnvironmentHelper.IsLinux()) {
                if (EnvironmentHelper.IsArm()) {
                    platform = "arm";
                } else {
                    platform = "linux";
                }
            } else if (EnvironmentHelper.IsMac()) {
                platform = "mac";
            }
            platform += EnvironmentHelper.Is64bit() ? "64" : "32";
            LogIt.Info("Sending registration request for agent[{0}] to Hub [{1}]",
                platform, this.getAppConfiguration().agent.hub);
            const domain : string = this.getHttpManager().getRequest().getServerIP();
            let name : string = this.getEnvironmentArgs().getAppName();
            if (!ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().agent.name)) {
                name = this.getAppConfiguration().agent.name;
            }
            if (!ObjectValidator.IsEmptyOrNull(args.AgentName())) {
                name = args.AgentName();
            }
            let poolName : string = this.getAppConfiguration().agent.poolName;
            if (ObjectValidator.IsEmptyOrNull(poolName)) {
                poolName = "Default";
            }
            let tag : string = this.getAppConfiguration().agent.tag;
            if (ObjectValidator.IsEmptyOrNull(tag)) {
                tag = "";
            }
            const metadata : IAgentMetadata = {
                configuration,
                environment: {}
            };
            if (!this.getAppConfiguration().agent?.disableMetadataEnv) {
                metadata.environment = process.env;
            }
            connector.getEvents().OnStart(() : void => {
                connector
                    .RegisterAgent({
                        credentials: this.getAppConfiguration().agent.credentials,
                        domain,
                        metadata,
                        name,
                        packageInfo: {
                            buildTime: new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                            name     : this.getEnvironmentArgs().getProjectName(),
                            platform : this.getEnvironmentArgs().getPlatform(),
                            release  : this.getEnvironmentArgs().getReleaseName(),
                            version  : this.getEnvironmentArgs().getProjectVersion()
                        },
                        platform,
                        poolName,
                        tag,
                        version    : this.getEnvironmentArgs().getProjectVersion()
                    })
                    .OnMessage(($data : any, $taskId : string) : void => {
                        const proxyConnector : IRequestConnector = {
                            Send            : ($data : any) : void => {
                                if ((<any>connector).getClient().CommunicationIsRunning()) {
                                    connector.ForwardMessage($taskId, $data);
                                }
                            },
                            getOwnerId      : () : string => {
                                return connector.getId() + "";
                            },
                            getParentProcess: () : HttpServer => {
                                return this.getHttpServer();
                            }
                        };
                        const response : IResponse = new WebResponse(proxyConnector, $data);
                        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs("/liveContent/");
                        args.POST().Add($data, "LiveProtocol");
                        args.POST().Add(response, "LiveResponse");
                        args.POST().Add(JSON.stringify($data), "Protocol");
                        args.POST().Add(proxyConnector, "Connector");
                        this.getHttpResolver().ResolveRequest(args);
                    })
                    .Then(($status : boolean, $authMethods : IAuthorizedMethods[]) : void => {
                        LogIt.Debug("Authorized methods: {0}", $authMethods);
                        if (!$status) {
                            LogIt.Warning("Unable to register agent!");
                            this.Exit(-1);
                        } else {
                            LogIt.Info("Agent registered.");
                        }
                    });
            });
            connector.getEvents().OnClose(() : void => {
                connector.StartCommunication();
            });
            connector.getEvents().OnError(() : void => {
                setTimeout(() : void => {
                    connector.StartCommunication();
                }, 5000);
            });
            connector.StartCommunication();

            let timeoutHandle : any;
            const clearLimit : any = () => {
                if (!ObjectValidator.IsEmptyOrNull(timeoutHandle)) {
                    clearTimeout(timeoutHandle);
                }
            };
            const heartbeatPing : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.loopThread)) {
                    clearTimeout(this.loopThread);
                }
                this.loopThread = EventsManager.getInstanceSingleton().FireAsynchronousMethod(async () : Promise<void> => {
                    try {
                        const ask : any = async () : Promise<void> => {
                            const statusData : any = await connector.UpdateAgentStatus({status: "active"});
                            // todo(mkelnar) there are status data which could be used for some metrics or setting gathering
                            //  restart is currently connected with the same behaviour as heartbeat but restart flag
                            //  could be aligned with complete restart instead (review after integration)
                            if (statusData.hasOwnProperty("restart") && statusData.restart === true) {
                                throw new Error("Restart required by HUB");
                            }
                        };
                        await Promise.race([
                            ask(),
                            new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
                                timeoutHandle = setTimeout(() : void => {
                                    const error = new Error("Agent status update failed: timeout limit exceed.");
                                    $reject(error);
                                }, this.getAppConfiguration().heartbeat.timeout);
                            })
                        ]);
                    } catch (e) {
                        LogIt.Warning(e.message);
                        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() => {
                            if (this.getAppConfiguration().heartbeat.exitOnError) {
                                LogIt.Warning("Restarting application instance...");
                                this.Exit(1);
                            } else {
                                (<any>connector).getClient().StopCommunication();
                                // this could stay commented out while restart policy = auto
                                // connector.StartCommunication();
                            }
                        }, this.getAppConfiguration().heartbeat.restartDelay);
                    } finally {
                        clearLimit();
                    }
                    heartbeatPing();
                }, this.getAppConfiguration().heartbeat.period);
            };
            if (this.getAppConfiguration().heartbeat.enabled) {
                heartbeatPing();
            }
            return true;
        } else if (args.IsSnippet()) {
            new HeadlessSnippet().Process(() : void => {
                this.Exit(0);
            });
        } else {
            if (args.HostPort() === args.ServicesPort()) {
                LogIt.Info("Host and services ports are identical, using default values");
                args.ServicesPort(8888);
                args.HostPort(88);
            }
            return super.processProgramArgs();
        }
    }
}
