/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FrontEndLoader as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FrontEndLoader.js";

export class FrontEndLoader extends Parent {

    protected getLicense() : string {
        return `
<!--

Copyright 2014-2016 Freescale Semiconductor, Inc.
Copyright 2017-2019 NXP
Copyright ${StringUtils.YearFrom(2019)} Oidis

SPDX-License-Identifier: BSD-3-Clause
The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution

or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText

-->
`;
    }
}
