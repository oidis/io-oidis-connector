/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

export class XOriginInterface extends BaseHttpResolver {

    protected resolver() : void {
        const EOL : string = StringUtils.NewLine(false);
        StaticPageContentManager.Clear(true);
        StaticPageContentManager.Title("Oidis - REST services");
        StaticPageContentManager.License(
            "<!--" + EOL +
            EOL +
            "Copyright 2010-2013 Jakub Cieslar" + EOL +
            "Copyright 2014-2016 Freescale Semiconductor, Inc." + EOL +
            "Copyright 2017-2019 NXP" + EOL +
            "Copyright " + StringUtils.YearFrom(2019) + " Oidis" + EOL +
            EOL +
            "SPDX-License-Identifier: BSD-3-Clause" + EOL +
            "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution" + EOL +
            "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText" + EOL +
            EOL +
            "-->"
        );
        StaticPageContentManager.getHeadLinks().Clear();
        StaticPageContentManager.HeadScriptAppend("/resource/javascript/xservices.js?version=" +
            this.getEnvironmentArgs().getProjectVersion());
        StaticPageContentManager.BodyAppend("Oidis Cross-origin REST service interface");
        StaticPageContentManager.Draw();
    }
}
