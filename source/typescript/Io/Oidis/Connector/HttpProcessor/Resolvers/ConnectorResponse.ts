/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ConnectorResponse as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/ConnectorResponse.js";
import { IResponseProtocol } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Loader } from "../../Loader.js";

export class ConnectorResponse extends Parent {

    protected resolver() : void {
        if (StringUtils.Contains(this.RequestArgs().Url(), "/connector.config.js")) {
            const extension : string = Loader.getInstance().getProgramArgs().IsDebug() ? ".js" : ".jsonp";
            this.getConnector().Send(<IResponseProtocol>{
                body   : require("fs").createReadStream(
                    Loader.getInstance().getProgramArgs().ProjectBase() + "/connector.config" + extension),
                headers: {"content-type": "application/javascript"},
                status : HttpStatusType.SUCCESS
            });
        } else {
            super.resolver();
        }
    }
}
