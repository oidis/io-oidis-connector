/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LiveContentResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/LiveContentResolver.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Loader } from "../../Loader.js";

export class LiveContentResolver extends Parent {

    protected resolveCustomProtocol($type : string, $args : any, $response : IResponse) : void {
        switch ($type) {
        case "Cmd":
        case "Spawn":
            Loader.getInstance().getTerminal().Spawn($args.cmd, $args.args, $args.cwd, $response);
            break;

        case "FileSystem":
        case "FileSystemHandler":
            Loader.getInstance().getFileSystemHandler().Resolve($args, $response);
            break;

        default:
            super.resolveCustomProtocol($type, $args, $response);
            break;
        }
    }
}
