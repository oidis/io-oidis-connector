/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpResolver.js";

export class HttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        await this.registerResolver(async () => (await import("./Resolvers/FrontEndLoader.js")).FrontEndLoader,
            "/");
        await this.registerResolver(async () => (await import("./Resolvers/LiveContentResolver.js")).LiveContentResolver,
            "/liveContent/");
        await this.registerResolver(async () => (await import("./Resolvers/ConnectorResponse.js")).ConnectorResponse,
            "connector.config.jsonp", "connector.config.js");

        return super.getStartupResolvers($context);
    }
}
