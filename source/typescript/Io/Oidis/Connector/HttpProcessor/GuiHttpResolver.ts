/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { GuiHttpResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/GuiHttpResolver.js";

export class GuiHttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser) {
            await this.registerResolver(async () => {
                    if ($context.isProd) {
                        return (await import("./Resolvers/XOriginInterface.js")).XOriginInterface;
                    } else {
                        return (await import("../Index.js")).Index;
                    }
                },
                "/", "/index", "/index.html", "/web/");
        }
        return super.getStartupResolvers($context);
    }
}
