/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IndexPage } from "@io-oidis-gui/Io/Oidis/Gui/Pages/IndexPage.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { Snippets } from "./RuntimeTests/Snippets.js";

export class Index extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Framework Connector");
        $instance.headerInfo.Content("Standalone HTTP server for Oidis Framework applications " +
            "with WebSockets support and built-in web host");

        let content : string = "";
        /* dev:start */
        content += `
            <h3>Runtime tests</h3>
            <a href="${Snippets.CallbackLink()}">Snippets</a>
            `;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}
