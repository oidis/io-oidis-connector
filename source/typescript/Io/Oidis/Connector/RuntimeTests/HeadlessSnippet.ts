/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";

export class HeadlessSnippet extends BaseObject {

    public Process($done : () => void) : void {
        /* dev:start */
        /* THIS TASK IS ONLY FOR DEVELOPMENT, DO NOT COMMIT CHANGES HERE */
        // cli usage: [target_name][.exe] snippet

        LogIt.Info("Hello from Connector snippet");
        /* dev:end */
        $done();
    }
}
