/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { FileSystemHandlerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";

export class Snippets extends RuntimeTestRunner {

    public testAutoconect() : void {
        const connector : FileSystemHandlerConnector = new FileSystemHandlerConnector(false,
            this.getRequest().getHostUrl() + "connector.config.jsonp", WebServiceClientType.DESKTOP_CONNECTOR);
        connector.getTempPath().Then(($path : string) : void => {
            Echo.Printf($path);
        });
    }
}
/* dev:end */
