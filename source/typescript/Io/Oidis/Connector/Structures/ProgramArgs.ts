/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ProgramArgs as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Structures/ProgramArgs.js";

export class ProgramArgs extends Parent {
    private hostPort : number;
    private servicesPort : number;
    private openHost : boolean;
    private configLocations : string[];
    private isAgent : boolean;
    private agentName : string;
    private hubLocation : string;
    private snippet : boolean;
    private userName : string;
    private token : string;
    private groups : string[];

    constructor() {
        super();
        this.hostPort = 0;
        this.servicesPort = 8888;
        this.openHost = false;
        this.configLocations = [];
        this.isAgent = false;
        this.snippet = false;
        this.agentName = "";
        this.hubLocation = "";
        this.userName = "";
        this.token = "";
        this.groups = [];
        this.StartServer(true);
    }

    public HostPort($value? : number) : number {
        return this.hostPort = Property.PositiveInteger(this.hostPort, $value, 1);
    }

    public ServicesPort($value? : number) : number {
        return this.servicesPort = Property.PositiveInteger(this.servicesPort, $value, 1);
    }

    public OpenHost($value? : boolean) : boolean {
        return this.openHost = Property.Boolean(this.openHost, $value);
    }

    public ConfigLocations($value? : string[]) : string[] {
        if (ObjectValidator.IsSet($value)) {
            this.configLocations = $value;
        }
        return this.configLocations;
    }

    public IsAgent($value? : boolean) : boolean {
        return this.isAgent = Property.Boolean(this.isAgent, $value);
    }

    public AgentName($value? : string) : string {
        return this.agentName = Property.String(this.agentName, $value);
    }

    public HubLocation($value? : string) : string {
        return this.hubLocation = Property.String(this.hubLocation, $value);
    }

    public IsSnippet($value? : boolean) : boolean {
        return this.snippet = Property.Boolean(this.snippet, $value);
    }

    public UserName($value? : string) : string {
        return this.userName = Property.String(this.userName, $value);
    }

    public Token($value? : string) : string {
        return this.token = Property.String(this.token, $value);
    }

    public Groups($value? : string[]) {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.groups = $value;
        }
        return this.groups;
    }

    public PrintHelp() : void {
        this.getHelp(
            "" +
            "Oidis Framework Connector                                                    \n" +
            "   - Standalone HTTP server for with built-in web host and WebSockets support\n" +
            "                                                                             \n" +
            "   copyright:         Copyright 2014-2016 Freescale Semiconductor, Inc.,     \n" +
            "                      Copyright 2017-2019 NXP                                \n" +
            "                      Copyright " + StringUtils.YearFrom(2019) + " Oidis                              \n" +
            "   author:            Oidis z.s., info@oidis.io                              \n" +
            "   license:           BSD-3-Clause License distributed with this material    \n",

            "" +
            "Basic options:                                                               \n" +
            "   -h [ --help ]               Prints application help description.          \n" +
            "   -v [ --version ]            Prints application version message.           \n" +
            "   -p [ --path ]               Print current Oidis Connector location.       \n" +
            "                                                                             \n" +
            "Server options:                                                              \n" +
            "   --stop [ -s ]               Stop localhost service.                       \n" +
            "   --open-host [ -o ]          Use this to open host server.                 \n" +
            "   agent                       Connect Oidis Connector to Oidis Hub.         \n" +
            "                                                                             \n" +
            "Other options:                                                               \n" +
            "   --services-port=<value>     Specify server services port.                 \n" +
            "   --host-port=<value>         Specify host port.                            \n" +
            "   --name=<value>              Specify name used by Oidis Hub registration.  \n" +
            "   --hub=<value>               Specify type or url for Oidis Hub.            \n" +
            "   --user=<value>              Specify UserName for Hub account.             \n" +
            "   --token=<value>             Specify authorization token for Hub account.  \n" +
            "   --groups=<value>            Semicolon separated list of group IDs         \n" +
            "   --target=<path>             Specify target *.html file path.              \n" +
            "   --config-locations=<paths>  Specify list of connector.config.jsonp        \n" +
            "                                 locations separated by ';'.                 \n"
        );
    }

    protected processDefaultArgs() : void {
        this.StartServer(true);
    }

    protected processArg($key : string, $value : string) : boolean {
        switch ($key) {
        case "--stop":
        case "-s":
            this.StopServer(true);
            break;
        case "--open-host":
        case "-o":
            this.OpenHost(true);
            break;
        case "--services-port":
            this.ServicesPort(StringUtils.ToInteger($value));
            break;
        case "--host-port":
            this.HostPort(StringUtils.ToInteger($value));
            break;
        case "--config-locations":
            if (StringUtils.Contains($value, ";")) {
                this.ConfigLocations(StringUtils.Split($value, ";"));
            } else {
                this.ConfigLocations([$value]);
            }
            break;
        case "agent":
            this.IsAgent(true);
            break;
        case "snippet":
            this.IsSnippet(true);
            break;
        case "--name":
            this.AgentName($value);
            break;
        case "--hub":
            this.HubLocation($value);
            break;
        case "--user":
        case "--username":
        case "--userName":
            this.UserName($value);
            break;
        case "--token":
        case "--authtoken":
        case "--authToken":
            this.Token($value);
            break;
        case "--groups":
            this.Groups(StringUtils.Split($value, ";", ":"));
            break;
        default:
            return super.processArg($key, $value);
        }
        return true;
    }
}
