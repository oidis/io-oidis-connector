/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IProject as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IProject.js";
import { ICredentials } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";

export interface IProject extends Parent {
    agent : IAgentConfiguration;
    hubLocations : IHubLocations;
    heartbeat : IHeartbeat;
}

export interface IAgentConfiguration {
    name : string;
    hub : string;
    credentials : ICredentials;
    poolName : string;
    tag : string;
    configuration : any;
    disableMetadataEnv : boolean;
}

export interface IHubLocations {
    prod : string;
    eap : string;
    dev : string;
}

export interface IHeartbeat {
    enabled : boolean;
    exitOnError : boolean;
    period : number;
    timeout : number;
    restartDelay : number;
}

// generated-code-start
/* eslint-disable */
export const IProject = globalThis.RegisterInterface(["agent", "hubLocations", "heartbeat"], <any>Parent);
export const IAgentConfiguration = globalThis.RegisterInterface(["name", "hub", "credentials", "poolName", "tag", "configuration", "disableMetadataEnv"]);
export const IHubLocations = globalThis.RegisterInterface(["prod", "eap", "dev"]);
export const IHeartbeat = globalThis.RegisterInterface(["enabled", "exitOnError", "period", "timeout", "restartDelay"]);
/* eslint-enable */
// generated-code-end
